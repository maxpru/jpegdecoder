[1mdiff --git a/src/jpeg/DataUnitDecoder.java b/src/jpeg/DataUnitDecoder.java[m
[1mindex ba61881..e3da832 100644[m
[1m--- a/src/jpeg/DataUnitDecoder.java[m
[1m+++ b/src/jpeg/DataUnitDecoder.java[m
[36m@@ -6,170 +6,59 @@[m [mimport main.Hexifier;[m
 [m
 public class DataUnitDecoder {[m
 [m
[32m+[m	[32m// References in comments refer to Annex F of ISO/IEC 10918-1 : 1993(E) as available at https://www.w3.org/Graphics/JPEG/itu-t81.pdf.[m
[32m+[m[41m	[m
 	public DataUnitDecoder(byte[] data, int codeStart, int precision) {[m
 		index = codeStart -1;[m
 		this.data = data;[m
 		this.precision = precision;[m
 	}[m
 [m
[32m+[m	[32mfinal static int SAMPLES_PER_DATA_UNIT = 64;[m
 	int index;[m
 	final byte[] data;[m
 	final int precision;[m
[32m+[m[41m	[m
[32m+[m	[32m//Initializing arrays early for speed.[m
[32m+[m	[32mint[] vals = new int[SAMPLES_PER_DATA_UNIT];[m
[32m+[m	[32mfloat[] floatVals = new float[SAMPLES_PER_DATA_UNIT];[m
 [m
[31m-	public int lastPred;[m
[32m+[m	[32mpublic int lastPred = 0;[m
 [m
[31m-	public void newMcu() {[m
[32m+[m	[32mpublic void startNewDataUnit() {[m
 		lastPred = 0;[m
 	}[m
[31m-[m
[31m-	private byte[] values(int start, int end) {[m
[31m-		return Arrays.copyOfRange(data, start, end);[m
[31m-	}[m
[31m-[m
[32m+[m[41m	[m
 	private byte value(int index) {[m
 		return data[index];[m
 	}[m
[31m-[m
[31m-	byte[] next(DataUnitTables tables, int pred) {[m
[31m-		byte[] out = new byte[64];[m
[31m-		int[] ints = nextAsInt(tables, pred);[m
[31m-		for (int i = 0; i < 64; i++) {[m
[31m-			out[i] = (byte) ints[i];[m
[31m-		}[m
[31m-		//		System.out.println("Stopped at byte " + index);[m
[31m-		return out;[m
[31m-	}[m
 	[m
[31m-	DecodedDataUnit nextAsDataUnit(DataUnitTables tables, int pred, String name) {[m
[31m-[m
[31m-		DecodedDataUnit out = new DecodedDataUnit();[m
[31m-		out.byteStart = index;[m
[31m-		out.positionInByteAtStart = 8 - nextbitCount;[m
[31m-		out.pred = pred;[m
[31m-		//		System.out.println("Data starting at " + (index + 1));[m
[31m-		//		System.out.println("Old value = " + pred);[m
[31m-[m
[31m-		//		for (int i = 1; i < 11; i++) {[m
[31m-		//			System.out.print(Hexifier.asBinary(value(index + i)) + " ");[m
[31m-		//		}[m
[31m-[m
[31m-		out.zaggedVals = getZZValues(pred, tables.dcTable, tables.acTable);[m
[31m-[m
[31m-		out.dezaggedVals = DeZagger.dezag(out.zaggedVals);[m
[31m-[m
[31m-		//		System.out.println("DeZagged values:");[m
[31m-		//		ArrayPrinter.print(vals);[m
[31m-[m
[31m-		float[] floatVals = new float[64];[m
[31m-[m
[31m-		//		System.out.println("SQT");[m
[31m-		//		ArrayPrinter.print(tables.scaledQuantizationTable.values);[m
[31m-[m
[31m-		for (int j = 0; j < 64; j++) {[m
[31m-			floatVals[j] = out.dezaggedVals[j] * tables.scaledQuantizationTable.values[j];[m
[31m-		}[m
[31m-[m
[31m-		Idct.inverseDCT8x8(floatVals);[m
[31m-[m
[31m-		out.afterIdct = floatVals;[m
[31m-[m
[31m-		//		System.out.println("IDCT complete.");[m
[31m-		//[m
[31m-		//		ArrayPrinter.print(floatVals);[m
[31m-		//[m
[31m-[m
[31m-		out.finalVals = inverseLevelShift(floatVals, precision);[m
[31m-		//		System.out.println("Final output.");[m
[31m-		//		ArrayPrinter.print(inverseLevelShifted);[m
[31m-[m
[31m-		out.name = name;[m
[31m-		out.qTableScaled = tables.scaledQuantizationTable;[m
[31m-		out.dcTable = tables.dcTable;[m
[31m-		out.acTable = tables.acTable;[m
[31m-[m
[31m-		return out;[m
[31m-	}[m
[31m-[m
[31m-	int[] nextAsInt(DataUnitTables tables, int pred) {[m
[31m-		//		System.out.println("Data starting at " + (index + 1));[m
[31m-		//		System.out.println("Old value = " + pred);[m
[31m-[m
[31m-		//		for (int i = 1; i < 11; i++) {[m
[31m-		//			System.out.print(Hexifier.asBinary(value(index + i)) + " ");[m
[31m-		//		}[m
[31m-[m
[31m-		int[] zaggedVals = getZZValues(pred, tables.dcTable, tables.acTable);[m
[31m-[m
[31m-		int[] vals = DeZagger.dezag(zaggedVals);[m
[31m-[m
[31m-		//		System.out.println("DeZagged values:");[m
[31m-		//		ArrayPrinter.print(vals);[m
[31m-[m
[31m-		float[] floatVals = new float[64];[m
[31m-[m
[31m-		//		System.out.println("SQT");[m
[31m-		//		ArrayPrinter.print(tables.scaledQuantizationTable.values);[m
[31m-[m
[31m-		for (int j = 0; j < 64; j++) {[m
[31m-			floatVals[j] = vals[j] * tables.scaledQuantizationTable.values[j];[m
[31m-		}[m
[31m-[m
[32m+[m	[32mint[] next(DataUnitTables tables, int pred) {[m
[32m+[m		[32mgetZZValues(pred, tables.dcTable, tables.acTable, vals);[m
[32m+[m		[32mvals = DeZagger.dezag(vals);[m
[32m+[m		[32mdequantize(tables, vals, floatVals);[m
 		Idct.inverseDCT8x8(floatVals);[m
[31m-		//		System.out.println("IDCT complete.");[m
[31m-		//[m
[31m-		//		ArrayPrinter.print(floatVals);[m
[31m-		//[m
[31m-		int[] inverseLevelShifted = inverseLevelShift(floatVals, precision);[m
[31m-		//		System.out.println("Final output.");[m
[31m-		//		ArrayPrinter.print(inverseLevelShifted);[m
[31m-[m
[31m-		return inverseLevelShifted;[m
[32m+[m		[32minverseLevelShift(floatVals, precision, vals);[m
[32m+[m		[32mreturn vals;[m
 	}[m
 [m
[31m-	int[] lineUp(int[][] in) {[m
[31m-		int[] out = new int[64];[m
[31m-		for (int i = 0; i < 8; i++) {[m
[31m-			for (int j = 0; j < 8; j++) {[m
[31m-				out[i*8 + j] = in[i][j];[m
[31m-			}[m
[31m-		}[m
[31m-		return out;[m
[31m-	}[m
[31m-[m
[31m-	int[] getZZValues(int pred, HuffmanTable dc, HuffmanTable ac) {[m
[31m-		int[] zz = new int[64];[m
[31m-		decodeDC(zz, pred, dc);[m
[31m-		decodeAC(zz, ac);[m
[31m-		//		System.out.println("Huffman decoding finished.");[m
[31m-		//		ArrayPrinter.print(zz);[m
[31m-		return zz;[m
[32m+[m	[32mvoid getZZValues(int pred, HuffmanTable dc, HuffmanTable ac, int[] out) {[m
[32m+[m		[32mdecodeDC(out, pred, dc);[m
[32m+[m		[32mdecodeAC(out, ac);[m
 	}[m
 [m
 	void decodeDC(int[] zz, int pred, HuffmanTable t) {[m
[32m+[m		[32m// F.2.2.1[m
 		int bitCount = decode(t);[m
 		int diff = receive(bitCount);[m
 		diff = extend(diff,bitCount);[m
[31m-		//		System.out.println("DIFF = " + diff);[m
[31m-		//		System.out.println("PRED = " + pred);[m
[31m-		int out = diff + pred;[m
[31m-		lastPred = out;[m
[31m-		//		System.out.println("DIFF * PRED = " + out);[m
[31m-		zz[0] = out;[m
[31m-	}[m
[31m-[m
[31m-	void decodeDC(int[] zz, int pred, HuffmanTable t, boolean debugging) {[m
[31m-		int bitCount = decode(t, true);[m
[31m-		int diff = receive(bitCount, true);[m
[31m-		diff = extend(diff,bitCount, true);[m
[31m-		System.out.println("DIFF = " + diff);[m
[31m-		System.out.println("PRED = " + pred);[m
 		int out = diff + pred;[m
 		lastPred = out;[m
[31m-		System.out.println("DIFF + PRED = " + out);[m
 		zz[0] = out;[m
 	}[m
 [m
 	void decodeAC(int[] zz, HuffmanTable t) {[m
[32m+[m		[32m// F.2.2.2[m
 		int index = 1;[m
 		while (index != 64) {[m
 			int bitCountAndIndexOffset = decode(t);[m
[36m@@ -186,16 +75,20 @@[m [mpublic class DataUnitDecoder {[m
 			index += indexOffset;[m
 			int zzk = receive(bitCount);[m
 			zz[index] = extend(zzk, bitCount);[m
[31m-			//			for (int i = 0; i < index + 1; i++) {[m
[31m-			//				System.out.println("ZZ(" + i + ") = " + zz[i]);[m
[31m-			//			}[m
 			index++;[m
 		}[m
 		}[m
 	}[m
 [m
[31m-	int[] inverseLevelShift(float[] vals, int framePrecision){[m
[31m-		int[] out = new int[64];[m
[32m+[m	[32mvoid dequantize(DataUnitTables tables, int[] in, float[] out) {[m
[32m+[m		[32m// F.2.1.4[m
[32m+[m		[32mfor (int j = 0; j < SAMPLES_PER_DATA_UNIT; j++) {[m
[32m+[m			[32mfloatVals[j] = vals[j] * tables.scaledQuantizationTable.values[j];[m
[32m+[m		[32m}[m
[32m+[m	[32m}[m
[32m+[m[41m	[m
[32m+[m	[32mvoid inverseLevelShift(float[] vals, int framePrecision, int[] out){[m
[32m+[m		[32m// F.2.1.5[m
 		int addend;[m
 		int max;[m
 		if (framePrecision == 8) {[m
[36m@@ -215,97 +108,21 @@[m [mpublic class DataUnitDecoder {[m
 			}[m
 			out[i] = Math.round(val);[m
 		}[m
[31m-		return out;[m
[31m-	}[m
[31m-[m
[31m-	byte[] rgb (int[] lum, int[] cb, int[] cr) {[m
[31m-		byte[] out = new byte[192];[m
[31m-		for (int i = 0; i < 64; i++) {[m
[31m-			out[i*3] = r(lum[i], cb[i], cr[i]);[m
[31m-			out[i*3+1] = g(lum[i], cb[i], cr[i]);[m
[31m-			out[i*3+2]  = b(lum[i], cb[i], cr[i]);[m
[31m-		}[m
[31m-		return out;[m
[31m-	}[m
[31m-[m
[31m-	int[] rgbCommented (int[] lum, int[] cb, int[] cr) {[m
[31m-		int[] out = new int[192];[m
[31m-		for (int i = 0; i < 64; i++) {[m
[31m-			System.out.println("Values in: " + lum[i] + ", " + cb[i] + ", " + cr[i]);[m
[31m-			out[i*3] = r(lum[i], cb[i], cr[i]);[m
[31m-			out[i*3+1] = g(lum[i], cb[i], cr[i]);[m
[31m-			out[i*3+2]  = b(lum[i], cb[i], cr[i]);[m
[31m-			int j = i*3;[m
[31m-			System.out.println("Values out: " + out[j++] + ", " + out[j++] + ", " + out[j]);[m
[31m-		}[m
[31m-		return out;[m
[31m-	}[m
[31m-[m
[31m-	byte r (int lum, int cb, int cr) {[m
[31m-		return (byte) Math.round(lum + 1.402 * (cr - 128));[m
 	}[m
 [m
[31m-	byte g (int lum, int cb, int cr) {[m
[31m-		return (byte) Math.round(lum - 0.34414 * (cb -128) - 0.71414 * (cr - 128));[m
[31m-	}[m
[31m-[m
[31m-	byte b (int lum, int cb, int cr) {[m
[31m-		return (byte) Math.round(lum + 1.772 * (cb - 128));[m
[31m-	}[m
[31m-[m
[31m-[m
[31m-[m
 	int nextbitCount = 0;[m
[31m-	byte nextbitBitsource;[m
[32m+[m	[32mbyte nextbitSource;[m
 	boolean nextbitSkip = false;[m
 [m
 	int nextbit() {[m
[32m+[m		[32m// F.2.2.5[m
 		int out = 0;[m
 		if (nextbitCount == 0) {[m
 			if (nextbitSkip == true) {[m
 				nextbitSkip = false;[m
 				index++;[m
 			}[m
[31m-			nextbitBitsource = value(++index);[m
[31m-			nextbitCount = 8;[m
[31m-			if (Hexifier.asHex(value(index)).equals("FF")) {[m
[31m-				byte next = value(index +1);[m
[31m-				if (next != 0) {[m
[31m-					if (JpegSymbol.byHex(Hexifier.asHex(next)) == JpegSymbol.DNL) {[m
[31m-						parseDnl();[m
[31m-						return -1;[m
[31m-					} else {[m
[31m-						throw new RuntimeException("Functional bytes " + Hexifier.asHex(Arrays.copyOfRange(data, index, index + 2)) + " found in image data.");[m
[31m-					}[m
[31m-				} else {[m
[31m-					nextbitSkip = true;[m
[31m-				}[m
[31m-			}[m
[31m-		}[m
[31m-[m
[31m-		Integer.toBinaryString(nextbitBitsource);[m
[31m-		//		System.out.println("Current byte: " + padBits(bitsrc, nextbitCount));[m
[31m-		if (nextbitBitsource < 0) {[m
[31m-			out = 1;[m
[31m-			nextbitBitsource += 128;[m
[31m-		}[m
[31m-		else {[m
[31m-			out = 0;[m
[31m-		}[m
[31m-		nextbitBitsource *= 2;[m
[31m-		nextbitCount--;[m
[31m-		//		System.out.println("Byte is now " + padBits(Integer.toBinaryString(nextbitBitsource), nextbitCount));[m
[31m-		return out;[m
[31m-	}[m
[31m-[m
[31m-	int nextbit(boolean debugging) {[m
[31m-		int out = 0;[m
[31m-		if (nextbitCount == 0) {[m
[31m-			if (nextbitSkip == true) {[m
[31m-				nextbitSkip = false;[m
[31m-				index++;[m
[31m-			}[m
[31m-			nextbitBitsource = value(++index);[m
[32m+[m			[32mnextbitSource = value(++index);[m
 			nextbitCount = 8;[m
 			if (Hexifier.asHex(value(index)).equals("FF")) {[m
 				byte next = value(index +1);[m
[36m@@ -322,23 +139,21 @@[m [mpublic class DataUnitDecoder {[m
 			}[m
 		}[m
 [m
[31m-		String bitsrc = Integer.toBinaryString(nextbitBitsource);[m
[31m-		System.out.println("Current byte: " + padBits(bitsrc, nextbitCount));[m
[31m-		if (nextbitBitsource < 0) {[m
[32m+[m		[32mInteger.toBinaryString(nextbitSource);[m
[32m+[m		[32mif (nextbitSource < 0) {[m
 			out = 1;[m
[31m-			nextbitBitsource += 128;[m
[32m+[m			[32mnextbitSource += 128;[m
 		}[m
 		else {[m
 			out = 0;[m
 		}[m
[31m-		nextbitBitsource *= 2;[m
[32m+[m		[32mnextbitSource *= 2;[m
 		nextbitCount--;[m
[31m-		System.out.println("Byte is now " + padBits(Integer.toBinaryString(nextbitBitsource), nextbitCount));[m
 		return out;[m
 	}[m
 [m
[31m-[m
 	String padBits(String bits, int length) {[m
[32m+[m		[32m// Grossly unoptimized.[m
 		String zeroes = "";[m
 		for (int i = 0; i < 8 - bits.length(); i++) {[m
 			zeroes = "0" + zeroes;[m
[36m@@ -352,57 +167,22 @@[m [mpublic class DataUnitDecoder {[m
 	}[m
 [m
 	int decode(HuffmanTable t) {[m
[31m-		//		System.out.println("Decoding.");[m
[31m-		int size = 0;[m
[31m-		int code = nextbit();[m
[31m-		//		System.out.println("Code = " + code);[m
[31m-		while (code > t.maxCode(size)) {[m
[31m-			//			System.out.println("Code > MaxCode(" + (size+1) + "), which is " + t.maxCode(size));[m
[31m-			size++;[m
[31m-			//			System.out.println("Increasing size to " + (size+1));[m
[31m-			code <<= 1;[m
[31m-			code += nextbit();[m
[31m-			//			System.out.println("Code is now " + String.format("%0" + (size+1) + "d",Long.parseLong(Integer.toBinaryString(code))));[m
[31m-		}[m
[31m-		int index = t.valPtr(size);[m
[31m-		index += code;[m
[31m-		index -= t.minCode(size);[m
[31m-		//		System.out.println("That makes the index " + index);[m
[31m-		//		System.out.println("Code for " + t.values[index]);[m
[31m-		return t.values[index];[m
[31m-	}[m
[31m-[m
[31m-	int decode(HuffmanTable t, boolean debugging) {[m
[31m-		System.out.println("Decoding.");[m
[32m+[m		[32m// F.2.2.3[m
 		int size = 0;[m
 		int code = nextbit();[m
[31m-		System.out.println("Code = " + code);[m
 		while (code > t.maxCode(size)) {[m
[31m-			System.out.println("Code > MaxCode(" + (size+1) + "), which is " + t.maxCode(size));[m
 			size++;[m
[31m-			System.out.println("Increasing size to " + (size+1));[m
 			code <<= 1;[m
 			code += nextbit();[m
[31m-			System.out.println("Code is now " + String.format("%0" + (size+1) + "d",Long.parseLong(Integer.toBinaryString(code))));[m
 		}[m
 		int index = t.valPtr(size);[m
 		index += code;[m
 		index -= t.minCode(size);[m
[31m-		System.out.println("That makes the index " + index);[m
[31m-		System.out.println("Code for " + t.values[index]);[m
 		return t.values[index];[m
 	}[m
 [m
 	int receive(int ssss) {[m
[31m-		int out = 0;[m
[31m-		for (int i = 0; i < ssss; i++) {[m
[31m-			out <<= 1;[m
[31m-			out += nextbit();[m
[31m-		}[m
[31m-		return out;[m
[31m-	}[m
[31m-[m
[31m-	int receive(int ssss, boolean debugging) {[m
[32m+[m		[32m// F.2.2.4[m
 		int out = 0;[m
 		for (int i = 0; i < ssss; i++) {[m
 			out <<= 1;[m
[36m@@ -412,27 +192,12 @@[m [mpublic class DataUnitDecoder {[m
 	}[m
 [m
 	int extend(int value, int size) {[m
[31m-		//		System.out.println("Extending " + value + " to size " + size);[m
[32m+[m		[32m// F.2.2.1[m
 		if (value < Math.pow(2, size-1)) {[m
 			int neg =  1 - (int)Math.pow(2, size);[m
[31m-			//			System.out.println("Result: " + (neg + value));[m
 			return neg + value;[m
 		} else {[m
[31m-			//			System.out.println("Stays the same.");[m
 			return value;[m
 		}[m
 	}[m
[31m-[m
[31m-	int extend(int value, int size, boolean debugging) {[m
[31m-		//		System.out.println("Extending " + value + " to size " + size);[m
[31m-		if (value < Math.pow(2, size-1)) {[m
[31m-			int neg =  1 - (int)Math.pow(2, size);[m
[31m-			//			System.out.println("Result: " + (neg + value));[m
[31m-			return neg + value;[m
[31m-		} else {[m
[31m-			//			System.out.println("Stays the same.");[m
[31m-			return value;[m
[31m-		}[m
[31m-	}[m
[31m-[m
 }[m
