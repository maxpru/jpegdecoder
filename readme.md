Takes as argument a file path and decodes and displays a JPEG file.

Only a subset of possible JPEG files is supported:

- The data must be compressed by the Baseline DCT process.
- Restart must be disabled.
- All compressed data must be stored in a single scan in a single frame.
- Data must be in YCbCr (and these components must be specified in this order.)
- This decoder loads the file into a single byte array, so the picture size in bytes must be smaller than the maximum array size (and loading will usually fail even before that because the VM will run out of memory.)

As far as I can tell, at least the first four points are fulfilled by the vast majority of jpeg files.