

// Class (minimally) adapted from org.apache.commons.imaging.formats.jpeg.decoder.Dct.
// This class always assumes the baseline DCT encoding with 8 bit precision.

public class Idct {

	private static final float C2 = (float) (2.0 * Math.cos(Math.PI / 8));
	private static final float C4 = (float) (2.0 * Math.cos(2 * Math.PI / 8));
	private static final float C6 = (float) (2.0 * Math.cos(3 * Math.PI / 8));
	private static final float Q = C2 - C6;
	private static final float R = C2 + C6;

	private static final float[] IDCT_SCALING_FACTORS = {
			(float) (2.0 * 4.0 / Math.sqrt(2.0) * 0.0625),
			(float) (4.0 * Math.cos(Math.PI / 16.0) * 0.125),
			(float) (4.0 * Math.cos(2.0 * Math.PI / 16.0) * 0.125),
			(float) (4.0 * Math.cos(3.0 * Math.PI / 16.0) * 0.125),
			(float) (4.0 * Math.cos(4.0 * Math.PI / 16.0) * 0.125),
			(float) (4.0 * Math.cos(5.0 * Math.PI / 16.0) * 0.125),
			(float) (4.0 * Math.cos(6.0 * Math.PI / 16.0) * 0.125),
			(float) (4.0 * Math.cos(7.0 * Math.PI / 16.0) * 0.125), };

	public static void scaleDequantizationMatrix(final float[] matrix) {
		for (int y = 0; y < 8; y++) {
			for (int x = 0; x < 8; x++) {
				matrix[8 * y + x] *= IDCT_SCALING_FACTORS[y]
						* IDCT_SCALING_FACTORS[x];
			}
		}
	}

	public static void inverseDCT8x8(final float[] matrix) {
		float a2, a3, a4, tmp1, tmp2, a5, a6, a7;
		float tmp4, neg_b4, b6, b2, b5;
		float tmp3, n0, n1, n2, n3, neg_n5;
		float m3, m4, m5, m6, neg_m7;

		for (int i = 0; i < 8; i++) {
			a2 = matrix[8 * i + 2] - matrix[8 * i + 6];
			a3 = matrix[8 * i + 2] + matrix[8 * i + 6];
			a4 = matrix[8 * i + 5] - matrix[8 * i + 3];
			tmp1 = matrix[8 * i + 1] + matrix[8 * i + 7];
			tmp2 = matrix[8 * i + 3] + matrix[8 * i + 5];
			a5 = tmp1 - tmp2;
			a6 = matrix[8 * i + 1] - matrix[8 * i + 7];
			a7 = tmp1 + tmp2;
			tmp4 = C6 * (a4 + a6);
			neg_b4 = Q * a4 + tmp4;
			b6 = R * a6 - tmp4;
			b2 = a2 * C4;
			b5 = a5 * C4;
			tmp3 = b6 - a7;
			n0 = tmp3 - b5;
			n1 = matrix[8 * i] - matrix[8 * i + 4];
			n2 = b2 - a3;
			n3 = matrix[8 * i] + matrix[8 * i + 4];
			neg_n5 = neg_b4;
			m3 = n1 + n2;
			m4 = n3 + a3;
			m5 = n1 - n2;
			m6 = n3 - a3;
			neg_m7 = neg_n5 + n0;
			matrix[8 * i] = m4 + a7;
			matrix[8 * i + 1] = m3 + tmp3;
			matrix[8 * i + 2] = m5 - n0;
			matrix[8 * i + 3] = m6 + neg_m7;
			matrix[8 * i + 4] = m6 - neg_m7;
			matrix[8 * i + 5] = m5 + n0;
			matrix[8 * i + 6] = m3 - tmp3;
			matrix[8 * i + 7] = m4 - a7;
		}

		for (int i = 0; i < 8; i++) {
			a2 = matrix[16 + i] - matrix[48 + i];
			a3 = matrix[16 + i] + matrix[48 + i];
			a4 = matrix[40 + i] - matrix[24 + i];
			tmp1 = matrix[8 + i] + matrix[56 + i];
			tmp2 = matrix[24 + i] + matrix[40 + i];
			a5 = tmp1 - tmp2;
			a6 = matrix[8 + i] - matrix[56 + i];
			a7 = tmp1 + tmp2;
			tmp4 = C6 * (a4 + a6);
			neg_b4 = Q * a4 + tmp4;
			b6 = R * a6 - tmp4;
			b2 = a2 * C4;
			b5 = a5 * C4;
			tmp3 = b6 - a7;
			n0 = tmp3 - b5;
			n1 = matrix[i] - matrix[32 + i];
			n2 = b2 - a3;
			n3 = matrix[i] + matrix[32 + i];
			neg_n5 = neg_b4;
			m3 = n1 + n2;
			m4 = n3 + a3;
			m5 = n1 - n2;
			m6 = n3 - a3;
			neg_m7 = neg_n5 + n0;
			matrix[i] = m4 + a7;
			matrix[8 + i] = m3 + tmp3;
			matrix[16 + i] = m5 - n0;
			matrix[24 + i] = m6 + neg_m7;
			matrix[32 + i] = m6 - neg_m7;
			matrix[40 + i] = m5 + n0;
			matrix[48 + i] = m3 - tmp3;
			matrix[56 + i] = m4 - a7;
		}
	}
}
