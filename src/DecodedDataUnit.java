

public class DecodedDataUnit {
	public int[] zaggedVals;
	public int[] dezaggedVals;
	public float[] afterIdct;
	public int[] finalVals;
	public String name;
	public int byteStart;
	public int positionInByteAtStart;
	public ScaledQuantizationTable qTableScaled;
	public HuffmanTable dcTable;
	public HuffmanTable acTable;
	public int pred;

}
