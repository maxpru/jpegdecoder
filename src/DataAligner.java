

public class DataAligner {
	
	public static int DATA_UNIT_WIDTH = 8;
	public static int DATA_UNIT_HEIGHT = 8;
	public static int PIXEL_PER_DU = DATA_UNIT_WIDTH * DATA_UNIT_HEIGHT;
	
	final int Hmax;
	final int Vmax;
	
	final int totalDataUnits;
	
	final int totalWidth;
	
	final int yFactH, yFactV;
	final int cbFactH, cbFactV;
	final int crFactH, crFactV;
	
	int Hy, Vy, Hcb, Vcb, Hcr, Vcr;
	
	
	public DataAligner (int Hy, int Vy, int Hcb, int Vcb, int Hcr, int Vcr) {
		Hmax = Math.max(Math.max(Hy,  Hcb),  Hcr);
		Vmax = Math.max(Math.max(Vy,  Vcb), Vcr);
	
		totalDataUnits = Hmax * Vmax;
		totalWidth = Hmax * DATA_UNIT_WIDTH;
		
		this.Hy = Hy;
		this.Vy = Vy;
		this.Hcb = Hcb;
		this.Vcb = Vcb;
		this.Hcr = Hcr;
		this.Vcr = Vcr;
		
		yFactH = Hmax / Hy;
		yFactV = Vmax / Vy;
		cbFactH = Hmax / Hcb;
		cbFactV = Vmax / Vcb;
		crFactH = Hmax / Hcr;
		crFactV = Vmax / Vcr;
	}
	
	public int[] align(int[][] DataUnits, Component c) {
		
	
		int [] out = new int[totalDataUnits * PIXEL_PER_DU];
		
		concatenateAndLinearize(DataUnits, out, c);
		
		return out;
		

	}
			
	void concatenateAndLinearize(int[][] in, int[] out, Component c) {
		int hFact;
		int vFact;
		int h;
		
		switch (c) {
		case Y:
			hFact = yFactH;
			vFact = yFactV;
			h = Hy;
			break;
		case Cb:
			hFact = cbFactH;
			vFact = cbFactV;
			h = Hcb;
			break;
		case Cr:
			hFact = crFactH;
			vFact = crFactV;
			h = Hcr;
			break;
		default:
			throw new RuntimeException("No Component selected.");
		}
		
		
		for (int i = 0; i < in.length; i++) {
			for (int j = 0; j < PIXEL_PER_DU; j++) {
				for (int y = 0; y < vFact; y++) {
					for (int x = 0; x < hFact; x++) {
						
						int xOfDataUnit = i % h;
						int yOfDataUnit = i / h;
						
						int xOfPixel = (xOfDataUnit * DATA_UNIT_WIDTH + j % DATA_UNIT_WIDTH) * hFact + x;
						int yOfPixel = (yOfDataUnit * DATA_UNIT_HEIGHT + j / DATA_UNIT_WIDTH) * vFact + y;
						
						int index = yOfPixel * totalWidth + xOfPixel;
						
						out[index] = in[i][j];
					}
				}
			}
		}
	}
	
	public enum Component {
		Y,
		Cb,
		Cr;
	}
}
