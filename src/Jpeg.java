
import java.awt.image.*;

public class Jpeg extends ByteStructure {

	FrameValues frameValues;
	ScanValues scanValues;

	int encodedDataStart;

	QuantizationTable[] qtable = new QuantizationTable[4];
	ScaledQuantizationTable[] scaledQTables = new ScaledQuantizationTable[4];
	HuffmanTable[] dcHtable = new HuffmanTable[2];
	HuffmanTable[] acHtable = new HuffmanTable[2];

	ImgEncoding encoding;

	ImageComponent currentComponent;
	HuffmanTable[] currentTables;
	boolean lookingAtDct = true;

	final static byte MARKER_START = Hexifier.fromHex("FF");

	final static int HUFFMAN_TABLE_CLASS_DC = 0;
	final static int HUFFMAN_TABLE_CLASS_AC = 1;

	static final int BYTE_COUNT_LENGTH = 2;
	
	final static int Y_COMPONENT = 0;
	final static int CB_COMPONENT = 1;
	final static int CR_COMPONENT = 2;// Both JFIF and EXIF use YCbCr in this order.

	final static int DATA_UNIT_WIDTH = 8;
	final static int DATA_UNIT_HEIGHT = 8;
	final static int DATA_UNIT_PIXEL_COUNT = DATA_UNIT_WIDTH * DATA_UNIT_HEIGHT;
	
	final static int DCT_PRECISION = 8;
	
	WritableRaster raster;

	int componentIndex = 0;
	
	private Jpeg (byte[] bytes) {
		super(bytes, 0);
	}
	
	public static Jpeg make(byte[] bytes) {
		if (badStart(bytes)) {
			throw new RuntimeException("Not a valid JPEG File.");
		}
		Jpeg jpeg = new Jpeg(bytes);
		jpeg.read();
		return jpeg;
	}
	
	public void read() {
		int index = 2; // We already checked that bytes 0 and 1 are the SOI segment.
		while (index < data.length) {
			byte current = data[index];
			JpegSymbol symbol;
			if (current == MARKER_START) {
				current = data[++index];
				symbol = interpret(current);
			index++;	
			int segLength = Hexifier.asInt(dataAt(index, index + 1));
			if (symbol.isSof()) {
				processSOF(symbol, data, index);
			}
			switch (symbol) {
			case DQT :
				processDQT(data, index);
				break;
			case DHT :
				processDHT(data, index);
				break;
			case SOS :
				processSOS(data, index);
				break;
			case DRI :
				validateDRI(data, index);
				break;
			default :
				break;
			}
			index += segLength;
			} else index++;
		}
	}
	
	void setCurrentComponent(int i) {
		currentComponent = scanValues.components[i];
		currentTables = scanValues.huffmanTables[i];
	}

	DataUnitTables makeTables() {
		return new DataUnitTables(scaledQTables[currentComponent.qTable], currentTables[0], currentTables[1]);
	}
	
	int crop (int base, int offset, int max) {
		return Math.min(base, max - offset);
	}
	
	WritableRaster scanAsRaster (int start) {

		ImageComponent yComponent = scanValues.components[Y_COMPONENT];
		ImageComponent cbComponent = scanValues.components[CB_COMPONENT];
		ImageComponent crComponent = scanValues.components[CR_COMPONENT];
		
		final int Hmax = frameValues.highestHFactor();
		final int Vmax = frameValues.highestVFactor();
		
		final int Hy = yComponent.horizontalSamplingFactor;
		final int Vy = yComponent.verticalSamplingFactor;
		final int Hcb = cbComponent.horizontalSamplingFactor;
		final int Vcb = cbComponent.verticalSamplingFactor;
		final int Hcr = crComponent.horizontalSamplingFactor;
		final int Vcr = crComponent.verticalSamplingFactor;
		
		final int yDataUnitCount = Hy * Vy;
		final int cbDataUnitCount = Hcb * Vcb;
		final int crDataUnitCount = Hcr * Vcr;
		
		final int mcuWidth = Hmax * DATA_UNIT_WIDTH;
		final int mcuHeight = Vmax * DATA_UNIT_HEIGHT;
		
		final int pixelPerMcu = mcuWidth * mcuHeight;
		
		WritableRaster raster = Raster.createPackedRaster(DataBuffer.TYPE_INT,
				frameValues.width, frameValues.height, new int[] {
						0x00ff0000, 0x0000ff00, 0x000000ff }, null);

		DataUnitDecoder decoder = new DataUnitDecoder(data, start, frameValues.precision);
		DataAligner aligner = new DataAligner(Hy, Vy, Hcb, Vcb, Hcr, Vcr);

		int lastYPredicate = 0;
		int lastCbPredicate = 0;
		int lastCrPredicate = 0;
		
		for (int ymcu = 0; ymcu * mcuHeight < frameValues.height; ymcu++) {
			for (int xmcu = 0; xmcu * mcuWidth < frameValues.width; xmcu++) {
				int[][] yDataUnits = new int[yDataUnitCount][];
				
				setCurrentComponent(Y_COMPONENT);
				for (int i = 0; i < yDataUnitCount; i++) {
					yDataUnits[i] = decoder.next(makeTables(), lastYPredicate);
					lastYPredicate = decoder.lastPred;
				}
				
				setCurrentComponent(CB_COMPONENT);
				decoder.lastPred = 0;
				int [][] cbDataUnits = new int [cbDataUnitCount][];
				for (int i = 0; i < cbDataUnitCount; i++) {
					cbDataUnits[i] = decoder.next(makeTables(), lastCbPredicate);
					lastCbPredicate = decoder.lastPred;
				}
				
				setCurrentComponent(CR_COMPONENT);
				decoder.lastPred = 0;
				int[][] crDataUnits = new int [crDataUnitCount][];
				for (int i = 0; i < crDataUnitCount; i++) {
					crDataUnits[i] = decoder.next(makeTables(), lastCrPredicate);
					lastCrPredicate = decoder.lastPred;
				}
				
				int[] ys = aligner.align(yDataUnits, DataAligner.Component.Y);
				int[] cbs = aligner.align(cbDataUnits, DataAligner.Component.Cb);
				int[] crs = aligner.align(crDataUnits, DataAligner.Component.Cr);
				
				int[] rgbs = new int[pixelPerMcu];
				for (int i = 0; i < pixelPerMcu; i++) {
						rgbs[i] = RgbConverter.convert(ys[i], cbs[i], crs[i]);
					}
				int x = xmcu * mcuWidth;
				int y = ymcu * mcuHeight;
				int width = mcuWidth;
				int height = mcuHeight;
				width = crop(width, x, frameValues.width);
				height = crop(height, y, frameValues.height);
				raster.setDataElements(x, y,width,height,rgbs);				
				}
			}		
		return raster;
		}


	private boolean quantizationTablesToFill (int i) {
		return qtable.length > i && qtable[i] != null;
	}

	private void scaleQuantizationTables () {
		int i = 0;
		while (quantizationTablesToFill(i)) {
			scaledQTables[i] = qtable[i].getScaledQuantizationTable();
			i++;
		}
	}
	
	private void processSOF (JpegSymbol s, byte[] data, int segmentStart) {
		getEncoding(s);
		if (encoding != ImgEncoding.BASELINE_DCT) {
			throw new RuntimeException("This decoder only handles the baseline DCT encoding. This file is encoded using " + encoding.toString());
		}
		FrameHeader h = new FrameHeader(data, segmentStart);
		frameValues = h.getValues();
		scaleQuantizationTables();
	}

	private void processDQT (byte[] data, int segmentStart) {
		DQT dqt = new DQT(data, segmentStart);
		dqt.fillInQTable(qtable);
	}

	private void processDHT(byte[] data, int segmentStart) {
		DHT dht = new DHT(data, segmentStart);
		int tableClass = dht.getTableClass();
		int destination = dht.getDestination();
		if (tableClass == HUFFMAN_TABLE_CLASS_DC) {
			dcHtable[destination] = dht.readHuffmanTable();
		}
		else if (tableClass == HUFFMAN_TABLE_CLASS_AC) {
			acHtable[destination] = dht.readHuffmanTable();
		} else {
			throw new RuntimeException("Table class can only be " + HUFFMAN_TABLE_CLASS_DC + " or " + HUFFMAN_TABLE_CLASS_AC + ", not " + tableClass);
		}
	}

	private void processSOS(byte[] data, int segmentStart) {
		ScanHeader scanHeader = new ScanHeader(data, segmentStart);
		scanValues = scanHeader.getScanValues(frameValues, dcHtable, acHtable);
		raster = scanAsRaster (scanValues.firstDataByte);
	}

	private void validateDRI(byte[] data, int segmentStart) {
		int restartIntervalStart = segmentStart + 2;
		int restartIntervalEnd = segmentStart + 3;
		int restartInterval = Hexifier.asInt(dataAt(restartIntervalStart, restartIntervalEnd));
		if (restartInterval != 0) {
			throw new RuntimeException("Restart intervals not supported.");
		}
	}
	

	void getEncoding(JpegSymbol s) {
		switch(s) {
		case SOF0 :
			encoding = ImgEncoding.BASELINE_DCT;
			break;
		case SOF1 :
			encoding = ImgEncoding.EXTENDED_SEQUENTIAL_DCT;
			break;
		case SOF2 :
			encoding = ImgEncoding.PROGRESSIVE_DCT;
			break;
		case SOF3 :
			encoding = ImgEncoding.LOSSLESS;
			break;
		case SOF5 :
			encoding = ImgEncoding.DIFFERENTIAL_SEQUENTIAL_DCT;
			break;
		case SOF6 :
			encoding = ImgEncoding.DIFFERENTIAL_PROGRESSIVE_DCT;
			break;
		case SOF7 :
			encoding = ImgEncoding.DIFFERENTIAL_LOSSLESS;
			break;
		case SOF9 :
			encoding = ImgEncoding.EXTENDED_SEQUENTIAL_DCT_ARITHMETIC;
			break;
		case SOF10 :
			encoding = ImgEncoding.PROGRESSIVE_DCT_ARITHMETIC;
			break;
		case SOF11 :
			encoding = ImgEncoding.LOSSLESS_ARITHMETIC;
			break;
		case SOF13 :
			encoding = ImgEncoding.DIFFERENTIAL_SEQUENTIAL_DCT_ARITHMETIC;
			break;
		case SOF14 :
			encoding = ImgEncoding.DIFFERENTIAL_PROGRESSIVE_DCT_ARITHMETIC;
			break;
		case SOF15 :
			encoding = ImgEncoding.DIFFERENTIAL_LOSSLESS_ARITHMETIC;
			break;
		default:
			throw new RuntimeException(s + " is not a SOF marker.");
		}
	}

	JpegSymbol interpret(byte b) {
		return JpegSymbol.byHex(Hexifier.asHex(b));
	}


	boolean isSymbol(JpegSymbol s) {
		return (!(s == null) && !(s == JpegSymbol.NONE));
	}

	static boolean startsWithSOI (byte [] bytes) {
		return matches (0,1,JpegSymbol.SOI.bytes(), bytes);
	}

	static boolean badStart (byte [] bytes) {
		return !startsWithSOI(bytes);
	}
	
	public void display() {
		ImageDisplayer.displayRaster(raster);
	}
}
