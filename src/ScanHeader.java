

public class ScanHeader extends ByteStructure {

	public ScanHeader(byte[] data, int start) {
		super(data, start);
	}

	public ScanValues getScanValues(FrameValues frame, HuffmanTable[] dcTables, HuffmanTable[] acTables) {
		
		ScanValues s = new ScanValues();
		int headerSize = Hexifier.asInt(dataAt(0, 1));
		s.componentCount = Hexifier.asInt(dataAt(2));
		
		for (int i = 0; i < s.componentCount; i++) {
			s.components[i] = getComponent(Hexifier.asInt(dataAt(3 + i*2)), frame);
			byte tableDestinationByte = dataAt(4 + i*2);
			s.huffmanTables[i][0] = dcTables[Hexifier.firstShort(tableDestinationByte)];
			s.huffmanTables[i][1] = acTables[Hexifier.secondShort(tableDestinationByte)];
		}
		int currentPosition = 3 + s.componentCount * 2;
		
		int startOfSpecOrPredSelection = Hexifier.asInt(dataAt(currentPosition++));
		int endOfSpecSelection = Hexifier.asInt(dataAt(currentPosition++));
		
		// These last two values are irrelevant to all sequential DCT modes of operation, including the baseline DCT, the only mode of operation
		// handled by this decoder. They should be set to 0 and 63, respectively, but there is no need to check for that (since non-baseline
		// DCT-encoded files should have been rejected before this point).
		
		byte aByte = dataAt(currentPosition++);
		
		int appBitPosHigh = Hexifier.firstShort(aByte);
		int appBitPosLow = Hexifier.secondShort(aByte);
		
		// These last two values are only relevant in successive approximation, a progressive DCT mode of operation not handled by this decoder.
		// At least the last one should be always set to zero for baseline DCT. The first one should always be set to zero for the first scan.
		// I suspect baseline DCT always has exactly one scan per frame, but am not sure.
		
		s.firstDataByte = start + currentPosition;
		return s;
	}

	ImageComponent getComponent(int identifier, FrameValues frameValues) {
		for (ImageComponent component : frameValues.components) {
			if (component.identifier == identifier) {
				return component;
			}
		}
		throw new RuntimeException("No component with identifier " + identifier);
	}

}
