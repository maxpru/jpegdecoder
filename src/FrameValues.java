

public class FrameValues {
	int precision;
	int width;
	int height;
	int componentCount;
	ImageComponent[] components;

	int highestHFactor() {
		int out = 0;
		for (ImageComponent c : components) {
			out = Math.max(out, c.horizontalSamplingFactor);
		}
		return out;
	}

	int highestVFactor() {
		int out = 0;
		for (ImageComponent c : components) {
			out = Math.max(out, c.verticalSamplingFactor);
		}
		return out;
	}

}
