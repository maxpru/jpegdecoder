
import java.io.*;

import javax.swing.SwingUtilities;

public class Main {

	public static void main(String[] args) {		
		if (args.length == 0) {
			throw new RuntimeException("Need to pass a file path as argument");
		}
		String path = args[0];
		byte[] bytes = fileToByteArray(path);
		bytes = clean(bytes);
		Jpeg j = Jpeg.make(bytes);
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				j.display();
			}});
	}


	static byte[] fileToByteArray(String path) {
		File f = new File(path);
		long size = f.length();
		try (FileInputStream stream = new FileInputStream(new File(path))){
			int sizeInt = Math.toIntExact(size);
			byte[] b = new byte[sizeInt];
			stream.read(b);
			return b;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (OutOfMemoryError e) {
			throw new RuntimeException("File size exceeds VM limit.");
		} catch (ArithmeticException e) {
			throw new RuntimeException("File size too big; this program can only handle files up to less than " + Integer.MAX_VALUE + " bytes.");
		}
		return null;
	}

	private static byte[] clean(byte[] bytes) {
		byte[] cleaned = new byte[0];
		for (int i = bytes.length-1; i>0; i--) {
			if (bytes[i] != 0) {
				cleaned = new byte[i+1];
				break;
			}
		}
		for (int i = 0; i < cleaned.length; i++) {
			cleaned[i] = bytes[i];
		}
		return cleaned;
	}


}