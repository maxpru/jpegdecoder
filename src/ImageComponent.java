

public class ImageComponent {
	public final int identifier;
	public final int horizontalSamplingFactor;
	public final int verticalSamplingFactor;
	public final int qTable;

	public ImageComponent (int ident, int hor, int vert, int table) {
		identifier = ident;
		horizontalSamplingFactor = hor;
		verticalSamplingFactor = vert;
		qTable = table;
	}

	@Override
	public String toString() {
		return "ImageComponent " + identifier + " [" + horizontalSamplingFactor + "," + verticalSamplingFactor + "], " + qTable;
	}

	public int dataUnitCount() {
		return horizontalSamplingFactor * verticalSamplingFactor;
	}
}
