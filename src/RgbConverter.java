

// Conversions taken from JFIF specification v. 1.02.
// Adapted there from CCIR Recommendation 601. 

public class RgbConverter {
	public static int convert(int y, int cb, int cr) {
		int r = (int) (y + 1.402 * (cr-128));
		r = clamp(r);
		int g = (int) (y - 0.34414 * (cb-128) - 0.71414 * (cr-128));
		g = clamp(g);
		int b = (int) (y + 1.772 * (cb-128));
		b = clamp(b);
		r = r << 16;
		g = g << 8;
		int out = r | g | b;
		return out;
	}
	
	static int clamp (int in) {
		int out = Math.min(255, in);
		out = Math.max(0, out);
		return out;
	}
}
