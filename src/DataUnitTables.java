

public class DataUnitTables {
	public final ScaledQuantizationTable scaledQuantizationTable;
	public final HuffmanTable dcTable;
	public final HuffmanTable acTable;

	public DataUnitTables (ScaledQuantizationTable q, HuffmanTable dc, HuffmanTable ac) {
		scaledQuantizationTable = q;
		dcTable = dc;
		acTable = ac;
	}
}
