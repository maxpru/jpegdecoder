/* All types which refer to a set of bytes are subclasses of this.
 * Subclasses will share the same array of bytes, so e.g. a section that starts at byte 15
 * and is 5 bytes long will share the same array <data> but have its start value at 15.
 */

import java.util.Arrays;

public class ByteStructure {
	public final byte[] data;
	public final int start;


	public ByteStructure(byte[] bytes, int start) {
		data = bytes;
		this.start = start;
	}

	public byte[] dataAt (int start, int end) {
		return Arrays.copyOfRange(data, start+this.start, end+1+this.start);
	}

	public byte dataAt(int i) {
		return data[start+i];
	}

	public static byte[] bytes(int start, int end, byte[] array) {
		return Arrays.copyOfRange(array, start, end+1);
	}

	public boolean matches (int start, int end, String s) {
		return matches (this.start + start, this.start + end, s, data);
	}

	public static boolean matches (int start, int end, String s, byte[] b) {
		return Hexifier.asHex(bytes(start, end, b)).equals(s);
	}
	
}
