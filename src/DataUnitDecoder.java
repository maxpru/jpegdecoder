

import java.util.Arrays;

public class DataUnitDecoder {

	// References in comments refer to Annex F of ISO/IEC 10918-1 : 1993(E) as available at https://www.w3.org/Graphics/JPEG/itu-t81.pdf.
	
	public DataUnitDecoder(byte[] data, int codeStart, int precision) {
		index = codeStart -1;
		this.data = data;
		this.precision = precision;
	}

	final static int SAMPLES_PER_DATA_UNIT = 64;
	int index;
	final byte[] data;
	final int precision;
	
	//Initializing arrays early for speed.
	int[] vals = new int[SAMPLES_PER_DATA_UNIT];
	float[] floatVals = new float[SAMPLES_PER_DATA_UNIT];

	public int lastPred = 0;

	public void startNewDataUnit() {
		lastPred = 0;
	}
	
	private byte value(int index) {
		return data[index];
	}
	
	int[] next(DataUnitTables tables, int pred) {
		vals = getZZValues(pred, tables.dcTable, tables.acTable);
		vals = DeZagger.dezag(vals);
		dequantize(tables, vals, floatVals);
		Idct.inverseDCT8x8(floatVals);
		inverseLevelShift(floatVals, precision, vals);
		return vals;
	}

	int [] getZZValues(int pred, HuffmanTable dc, HuffmanTable ac) {
		int[] out = new int[SAMPLES_PER_DATA_UNIT];
		decodeDC(out, pred, dc);
		decodeAC(out, ac);
		return out;
	}

	void decodeDC(int[] zz, int pred, HuffmanTable t) {
		// F.2.2.1
		int bitCount = decode(t);
		int diff = receive(bitCount);
		diff = extend(diff,bitCount); 
		int out = diff + pred;
		lastPred = out;
		zz[0] = out;
	}

	void decodeAC(int[] zz, HuffmanTable t) {
		// F.2.2.2
		int index = 1;
		while (index != 64) {
			int bitCountAndIndexOffset = decode(t);
			int bitCount = bitCountAndIndexOffset % 16;
			int indexOffset = bitCountAndIndexOffset >>> 4;
		if (bitCount == 0) {
			if (indexOffset == 15) {
				index += 16;
			} else {
				return;
			}
		}
		else {
			index += indexOffset;
			int zzk = receive(bitCount);
			zz[index] = extend(zzk, bitCount);
			index++;
		}
		}
	}

	void dequantize(DataUnitTables tables, int[] in, float[] out) {
		// F.2.1.4
		for (int j = 0; j < SAMPLES_PER_DATA_UNIT; j++) {
			floatVals[j] = vals[j] * tables.scaledQuantizationTable.values[j];
		}
	}
	
	void inverseLevelShift(float[] vals, int framePrecision, int[] out){
		// F.2.1.5
		int addend;
		int max;
		if (framePrecision == 8) {
			addend = 128;
			max = 255;
		} else {
			addend = 2048;
			max = 4095;
		}
		for (int i = 0; i < 64; i++) {
			float val = vals[i] + addend;
			if (val < 0) {
				val = 0;
			}
			if (val > max) {
				val = max;
			}
			out[i] = Math.round(val);
		}
	}

	int nextbitCount = 0;
	byte nextbitSource;
	boolean nextbitSkip = false;

	int nextbit() {
		// F.2.2.5
		int out = 0;
		if (nextbitCount == 0) {
			if (nextbitSkip == true) {
				nextbitSkip = false;
				index++;
			}
			nextbitSource = value(++index);
			nextbitCount = 8;
			if (Hexifier.asHex(value(index)).equals("FF")) {
				byte next = value(index +1);
				if (next != 0) {
					if (JpegSymbol.byHex(Hexifier.asHex(next)) == JpegSymbol.DNL) {
						parseDnl();
						return -1;
					} else {
						throw new RuntimeException("Functional bytes " + Hexifier.asHex(Arrays.copyOfRange(data, index, index + 2)) + " found in image data.");
					}
				} else {
					nextbitSkip = true;
				}
			}
		}

		Integer.toBinaryString(nextbitSource);
		if (nextbitSource < 0) {
			out = 1;
			nextbitSource += 128;
		}
		else {
			out = 0;
		}
		nextbitSource *= 2;
		nextbitCount--;
		return out;
	}

	String padBits(String bits, int length) {
		// Grossly unoptimized.
		String zeroes = "";
		for (int i = 0; i < 8 - bits.length(); i++) {
			zeroes = "0" + zeroes;
		}
		String out = zeroes + bits;
		return out.substring(out.length()-8,out.length()-8+length);
	}

	void parseDnl() {
		throw new RuntimeException("parseDnl() is not implemented yet.");
	}

	int decode(HuffmanTable t) {
		// F.2.2.3
		int size = 0;
		int code = nextbit();
		while (code > t.maxCode(size)) {
			size++;
			code <<= 1;
			code += nextbit();
		}
		int index = t.valPtr(size);
		index += code;
		index -= t.minCode(size);
		return t.values[index];
	}

	int receive(int ssss) {
		// F.2.2.4
		int out = 0;
		for (int i = 0; i < ssss; i++) {
			out <<= 1;
			out += nextbit();
		}
		return out;
	}

	int extend(int value, int size) {
		// F.2.2.1
		if (value < Math.pow(2, size-1)) {
			int neg =  1 - (int)Math.pow(2, size);
			return neg + value;
		} else {
			return value;
		}
	}
}
