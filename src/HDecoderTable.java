

public class HDecoderTable {
	int minCode[] = new int[16];
	int maxCode[] = new int[16];
	int firstValIndex[] = new int[16];
	HuffmanTable table;

	public HDecoderTable(HuffmanTable t) {
		table = t;
		int pos = 0;
		for (int size = 0; size < 16; size++) {
			if (table.numberOfCodesForLength[size] == 0) {
				maxCode[size] = -1;
			}
			else {
				firstValIndex[size] = pos;
				minCode[size] = table.codes[pos];
				pos += table.numberOfCodesForLength[size] -1;
				maxCode[size] = table.codes[pos];
				pos++;
			}
		}
	}

	public void print() {
		for (int i = 0; i< 16; i++) {
			System.out.println("Size " + (i+1));
			System.out.println("First value at: " + (firstValIndex[i]));
			System.out.println("Lowest code: " + Integer.toBinaryString(minCode[i]));
			if (maxCode[i] == -1) {
				System.out.println("Highest code: -1");
			} else {
				System.out.println("Highest code: " + Integer.toBinaryString(maxCode[i]));
			}
		}
	}
}
