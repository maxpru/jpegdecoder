

public enum JpegSymbol {

	SOF0("C0"),
	SOF1("C1"),
	SOF2("C2"),
	SOF3("C3"),

	SOF5("C5"),
	SOF6("C6"),
	SOF7("C7"),

	JPG("C8"),
	SOF9("C9"),
	SOF10("CA"),
	SOF11("CB"),

	SOF13("CD"),
	SOF14("CE"),
	SOF15("CF"),

	DHT("C4"),

	DAC("CC"),

	RST0("D0"),
	RST1("D1"),
	RST2("D2"),
	RST3("D3"),
	RST4("D4"),
	RST5("D5"),
	RST6("D6"),
	RST7("D7"),

	SOI("D8"),
	EOI("D9"),
	SOS("DA"),
	DQT("DB"),
	DNL("DC"),
	DRI("DD"),
	DHP("DE"),
	EXP("DF"),

	APP0("E0"),
	APP1("E1"),
	APP2("E2"),
	APP3("E3"),
	APP4("E4"),
	APP5("E5"),
	APP6("E6"),
	APP7("E7"),
	APP8("E8"),
	APP9("E9"),
	APP10("EA"),
	APP11("EB"),
	APP12("EC"),
	APP13("ED"),
	APP14("EE"),
	APP15("EF"),

	JPG0("F0"),
	JPG1("F1"),
	JPG2("F2"),
	JPG3("F3"),
	JPG4("F4"),
	JPG5("F5"),
	JPG6("F6"),
	JPG7("F7"),
	JPG8("F8"),
	JPG9("F9"),
	JPG10("FA"),
	JPG11("FB"),
	JPG12("FC"),
	JPG13("FD"),
	COM("FE"),

	TEM("01"),
	RES("Other"),

	FILL("FF"),

	NONE("");

	String hex;

	JpegSymbol(String hex){
		this.hex = hex;
	}

	public static JpegSymbol byHex(String s) {
		for (JpegSymbol sym : JpegSymbol.values()) {
			if (sym.hex.equals(s)) {
				return sym;
			}
		}
		if (s.length() == 2) {
			try {
				Integer.parseInt(s,16);
				return RES;
			}
			catch (NumberFormatException e) {
				throw new RuntimeException(s + " is not a valid byte value.");
			}
		}
		throw new RuntimeException(s + " is not a valid byte value.");
	}

	public String bytes() {
		return "FF" + hex;
	}

	public boolean isSof() {
		return ordinal() < 14 && this != JPG;
	}

	public boolean isRst() {
		return ordinal() > 15 && ordinal() < 24;
	}
}
