

public class QuantizationTable {

	public int[] values;

	static QuantizationTable getQuantizationTable(int precision, byte[] bytes) {
		if (precision == 0 && bytes.length == 64) {
			return new QuantizationTable8(bytes);
		}
		else if (precision == 1 && bytes.length == 128) {
			return new QuantizationTable16(bytes);
		} else {
			throw new RuntimeException("Can't have Quantization Table with " + precision + " precision and " + bytes.length + " bytes.");
		}
	}

	public ScaledQuantizationTable getScaledQuantizationTable() {
		float[] scaledVals = new float[64];
		for (int i = 0; i < 64; i++) {
			scaledVals[i] = values[i]; // Has to be done by iteration since scaledTable.values is a float[].
		}
		Idct.scaleDequantizationMatrix(scaledVals);
		ScaledQuantizationTable scaledTable = new ScaledQuantizationTable(scaledVals);
		return scaledTable;
	}

	private static class QuantizationTable8 extends QuantizationTable{
		QuantizationTable8(byte[] bytes) {
			values = new int[64];
			for (int i = 0; i < 64; i++) {
				values[i] = Hexifier.asInt(bytes[i]);
			}
			values = DeZagger.dezag(values);
		}
	}

	private static class QuantizationTable16 extends QuantizationTable{
		QuantizationTable16(byte[] bytes) {
			throw new RuntimeException("Quantization Tables for precision 1 not supported yet.");
		}
	}
}
