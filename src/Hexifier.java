public class Hexifier {

	public static String asHex(byte[] bytes) {
		StringBuilder sb = new StringBuilder();
		for (byte b : bytes) {
			sb.append(asHex(b));
		}
		return sb.toString();
	}

	public static String asHex(byte b) {
		return String.format("%02X", b);
	}

	public static byte fromHex(String s) {
		int i = Integer.parseInt(s, 16);
		byte b = (byte)(i);
		return b;
	}

	public static int asInt(byte[] bytes) {
		String s = asHex(bytes);
		return Integer.parseInt(s, 16);
	}

	public static int asInt(byte b) {
		String s = asHex(b);
		return Integer.parseInt(s,16);
	}


	public static int firstShort (byte b) {
		return Integer.parseInt(asHex(b).substring(0,1), 16);
	}

	public static int secondShort (byte b) {
		return Integer.parseInt(asHex(b).substring(1,2),16);
	}
}
