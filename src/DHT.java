public class DHT extends ByteStructure{

	public DHT(byte[] data, int start) {
		super(data, start);
	}

	public int getTableClass() {
		byte tableClassByte = dataAt(2);
		int tableClass = Hexifier.firstShort(tableClassByte);
		return tableClass;
	}

	public int getDestination() {
		byte destinationByte = dataAt(2);
		int destination = Hexifier.secondShort(destinationByte);
		return destination;
	}

	public HuffmanTable readHuffmanTable() {
		int size = Hexifier.asInt(dataAt(0, 1));
		int ls[] = new int[16];
		int total = 0;
		for (int i = 0; i < 16; i++) {
			ls[i] = Hexifier.asInt(dataAt(3 + i));
			total += ls[i];
		}
		HuffmanTable t = new HuffmanTable(ls);
		int vals[] = new int[total];
		for (int i = 0; i + 19 < size; i++) {
			vals[i] = Hexifier.asInt(dataAt(i + 19));
		}
		t.putValues(vals);
		t.makeDecoderTable();
		return t;
	}
}
