

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DirectColorModel;
import java.awt.image.WritableRaster;
import java.util.Properties;

import javax.swing.*;

public class ImageDisplayer {

	private static JFrame frame = new JFrame("Test");
	private static JPanel panel = new JPanel();

	public static void display(ColorModel cm, WritableRaster raster) {
		BufferedImage img = new BufferedImage(cm, raster,
				cm.isAlphaPremultiplied(), new Properties());

		display(img);
	}

	public static void displayRaster(WritableRaster raster) {
		ColorModel colorModel = new DirectColorModel(24, 0x00ff0000, 0x0000ff00,
				0x000000ff);
		display(colorModel, raster);
	}

	public static void display(BufferedImage img) {
		ImageIcon ic = new ImageIcon(img);
		JLabel label = new JLabel(ic);
		panel.add(label);

		JScrollPane sp = new JScrollPane(panel);
		frame.add(sp);
		frame.setLocationRelativeTo(null);
		frame.setSize(600,400);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.revalidate();
		frame.repaint();
	}
}