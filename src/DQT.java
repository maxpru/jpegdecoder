public class DQT extends ByteStructure {
	public DQT(byte[] data, int start) {
		super(data, start);
	}

	public void fillInQTable(QuantizationTable[] quantizationTables) {
		int size = Hexifier.asInt(dataAt(0, 1));
		int currentIndex = 2;
		while (currentIndex < size) {
			byte firstByte = dataAt(currentIndex);
			int precision = Hexifier.firstShort(firstByte);
			int destination = Hexifier.secondShort(firstByte);
			currentIndex ++;
			int additionalBytes = (precision == 1) ? 64 : 0;
			byte[] values = dataAt(currentIndex, currentIndex + 63 + additionalBytes);
			QuantizationTable t = QuantizationTable.getQuantizationTable(precision, values);
			currentIndex += (64 + additionalBytes);
			quantizationTables[destination] = t;
		}
	}
}
