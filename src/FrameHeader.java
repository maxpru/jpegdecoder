

public class FrameHeader extends ByteStructure {

	FrameHeader(byte[] data, int start){
		super(data, start);
	}

	FrameValues getValues() {
		int size = Hexifier.asInt(dataAt(0,1));
		FrameValues v = new FrameValues();
		v.precision = Hexifier.asInt(dataAt(2));
		v.height = Hexifier.asInt(dataAt(3, 4));
		v.width = Hexifier.asInt(dataAt(5, 6));
		v.componentCount = Hexifier.asInt(dataAt(7));
		v.components = new ImageComponent[v.componentCount];
		int currentIndex = 0;
		while (currentIndex < ((size - 8) / 3)) {
			int identifier = Hexifier.asInt(dataAt(8 + currentIndex * 3));
			byte hv = dataAt(9 + currentIndex * 3);
			int hFactor = Hexifier.firstShort(hv);
			int vFactor = Hexifier.secondShort(hv);
			int dest = Hexifier.asInt(dataAt(10 + currentIndex * 3));
			v.components[currentIndex] = new ImageComponent(identifier, hFactor, vFactor, dest);
			currentIndex++;
		}
		return v;
	}
}
