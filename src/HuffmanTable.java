

public class HuffmanTable {
	final int[] numberOfCodesForLength;
	int[] values;
	int[] codes;
	HDecoderTable decoder;

	public HuffmanTable(int[] numbers) {
		if (numbers.length != 16) {
			throw new RuntimeException("HuffmanTable should have 16 L values, not " + numbers.length + ".");
		}
		numberOfCodesForLength = numbers;
		values = new int[numbOfVals()];
		codes = generateCodes(numberOfCodesForLength);
	}

	public void putValues(int[] vals) {
		if(vals.length != numbOfVals()) {
			throw new RuntimeException("HuffmanTable should have " + numbOfVals() + " values, but it's fed " + vals.length + ".");
		}
		values = vals;
	}

	int numbOfVals() {
		int total = 0;
		for (int i : numberOfCodesForLength) {
			total += i;
		}
		return total;
	}

	@Override
	public String toString() {
		return "Huffman Table. Length: " + values.length;
	}

	public void print() {
		StringBuilder sb = new StringBuilder("");
		int ind = 0;
		for (int i = 0; i < 16; i++) {
			sb.append((i+1) + ": ");
			for (int j = 0; j < numberOfCodesForLength[i]; j++) {
				sb.append(values[ind] + " (" + String.format("%0" + (i+1) + "d",Long.parseLong(Integer.toBinaryString(codes[ind]))) + ") ");
				ind++;
			}
			sb.append("\r");
		}
		System.out.print(sb.toString());
	}

	int[] generateCodes(int[] codesPerLength) {
		int[] out = new int[numbOfVals()];
		int code = 0;
		int ind = 0;
		for (int size = 0; size < 16; size++) {
			int codes = codesPerLength[size];
			for (int i = 0; i < codes; i++) {
				out[ind++] = code;
				code++;
			}
			code *= 2;
		}
		return out;
	}

	void makeDecoderTable() {
		decoder = new HDecoderTable(this);
	}

	int maxCode(int size) {
		return decoder.maxCode[size];
	}

	int valPtr(int size) {
		return decoder.firstValIndex[size];
	}

	int minCode(int size) {
		return decoder.minCode[size];
	}
}
